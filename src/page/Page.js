import './Page.css'
import React from 'react'


const Page = ({ children }) => (
  <div className="page">
    <aside className="page__sidebar"></aside>
    <main className="page__main">{children}</main>
  </div>
)

export default Page
