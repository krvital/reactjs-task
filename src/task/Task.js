import './Task.css'
import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Icon  from '../icon/Icon'


const Task = (props) => {
  const taskCls = classnames('task', { 'task_active': props.active, 'task_editable': props.editable })

  if (!props.body && !props.services.length) {
    return props.editable ? (
      <div className={taskCls + ' task_empty'} onClick={props.onClick} tabIndex={1}>
        <div>
          <div className="task__time">{props.time}</div>
          <div>Empty slot</div>
          <div className="task__plus"><Icon name='plus' /></div>
        </div>
      </div>
    ) : null
  }

  return (
    <div
      tabIndex={props.editable ? '1' : '0'}
      className={taskCls}
      onClick={props.editable ? props.onClick : () => {}}
      onKeyPress={(e) => {
        if (props.editable && e.key === 'Enter') {
          props.onClick()
        }
      }}
    >
      <div className="task__clickable" onMouseOver={props.onMouseOver}/>
      <div className="task__backlayer"/>
      <div className="task__head">
        <div className="task__time">{props.time}</div>
        <div className="task__services">
          {props.services.map((service, index) => <Icon key={`${service}.${index}`} className="icon_social" name={service} />)}
        </div>
      </div>
      <div className="task__body">
        {props.body}
      </div>
      <div className="task__overlay" />
    </div>
  )
}

Task.propTypes = {
  onClick: PropTypes.func.isRequired,
  time: PropTypes.string,
  services: PropTypes.array,
  body: PropTypes.string,
}

export default Task
