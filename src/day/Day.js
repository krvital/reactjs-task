import './Day.css'
import React from 'react'
import PropTypes from 'prop-types'
import { weekday, dayName, isPast } from '../utils'
import Task from '../task/Task'
import NewTask from '../new-task/NewTask'
import Icon from '../icon/Icon'


export default class Day extends React.Component {
  static propTypes = {
    activeTaskId: PropTypes.string,
  }

  isTaskActive = (task) => this.props.activeTaskId === task.id

  render() {
    return (
      <div className="day" name={dayName(this.props.date).toLowerCase()}>
        <div className="day__head">
          <div className="day__title">
            <div className="day__name">{dayName(this.props.date)}</div>
            <div className="day__weekday">{weekday(this.props.date)}</div>
          </div>
          {this.props.stats && (
            <div className="day__stats">
              <span className="day__stat">
                <Icon name="readers" className="icon_stat"/>
                +{this.props.stats.readers}
              </span>
              <span className="day__stat">
                <Icon name="retweets" className="icon_stat"/>
                {this.props.stats.retweets}
              </span>
              <span className="day__stat">
                <Icon name="likes"  className="icon_stat"/>
                {this.props.stats.likes}
              </span>
              <span className="day__stat">
                <Icon name="replies" className="icon_stat"/>
                {this.props.stats.replies}
              </span>
            </div>
          )}
        </div>

        <div className="day__tasks">
          {this.props.tasks.map(task =>
            <Task
              key={task.id}
              onClick={this.props.onClickTask.bind(this, task.id)}
              onMouseOver={this.props.onMouseOver}
              editable={!isPast(this.props.date)}
              active={this.isTaskActive(task)}
              {...task}
            />
          )}
          {!isPast(this.props.date) && (
            <NewTask onClick={this.props.onClickNewTask} />
          )}
        </div>
      </div>
    )
  }
}
