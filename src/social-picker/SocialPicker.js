import './SocialPicker.css'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

const icons = {
  twitter: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAALlJREFUGBljDFj98T8DEYCJCDVgJWCFXCwMDJbSQAIKfFTYGJb58zJUW3MymEhCxMEKmZkYGQrNOBlyTDgYzKRYGPzV2Bg4WRmBilgZPv+CuAys/M+//wxff/9ncFZgA2OYyT///me49fYvmAs28fsfBobtd3/B5OH06Wd/GGA+hXtm74PfDNfeAHVAwbvv/xgWXPoB4zKAreYAkp7KbAzy/MwMzz7/BWr4y7D48k+GT1D3gVQzUj0cAS2XOTacgdTKAAAAAElFTkSuQmCC',
  gplus: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAANdJREFUGBljvBvk+J+BCMBEhBqwEhaYQnZVTQYOTR2Gn/fuMPy4ch4mDKfBJvK6+TKIZpUwMLJxMPA6eTAwcnBAFDAzMzAwQSwFmygYEsPwrCqXgYGVlYFNVpGBU8eI4fvVCwwiybkMf16/ZHi/cgEDRDkTM8O/X78YWIREGLiMLRhEs0sZGJlZGJg4ORmYuHnApoNN/LRtHYNERTPD5307gIqFGT4f3MXw78snhtdTuxn+//kNVsgICx4uEyuwZ349us/w5dAeBob/qKEGVwj3Hg4G0eEIAIkCOJJnAJ7/AAAAAElFTkSuQmCC',
  facebook: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAGVJREFUGBljdE9Y+p+BCMBEhBqwEgyF1sYyDBtmhDHE+OuimIGh0NFCgWHzvlsMSzZeRlHIgswDmWJjIgcWevv+O8OG3Tfh0igKQaaoKggx3H7wDkURSDWG1XAj0BhEK2SkejgCAClTGqKFDzTRAAAAAElFTkSuQmCC',
  instagram: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAPRJREFUGBlNkDFOw0AQRd/asU0CCFkUnIGOIyAqKioKJApukUNwE8oo1NQ0pE+fIjJUEK8RGDZ/1rbkkWbGntn58+e78DAP+CPwh9BYllsevK9PmO4gBBTAOeXB+lrsQffQerfX8JsJKYU2h9zQE3jcxMkkIhrqTP+LZ3jfyitYrgTzA9Mv9WohKkRrP+HyHKoPIWvqolT5Ww+t70arM02nKh6IWyKuhY4zxJm2yZL4YauLf1i/wOkESnF7e4UT5Uhr1yPagTZ9cw/1X3fU3ZWO8h2i1HDh6Sx0mh3rYq0ba9qo5sVXmuoYrR306zWLpIYQa449xypYYOj2DIMAAAAASUVORK5CYII=',
}

const services = [
  'twitter',
  'gplus',
  'facebook',
  'instagram'
]

export default class SocialPicker extends Component {
  static propTypes = {
    value: PropTypes.array,
    avatar: PropTypes.string,
  }

  static defaultProps = {
    value: []
  }

  constructor(props) {
    super(props)

    this.state = {
      selectedAccounts: {
        twitter: props.value.includes('twitter'),
        gplus: props.value.includes('gplus'),
        facebook: props.value.includes('facebook'),
        instagram: props.value.includes('instagram'),
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.value !== prevProps.value) {
      this.setState({
        selectedAccounts: {
          twitter: this.props.value.includes('twitter'),
          gplus: this.props.value.includes('gplus'),
          facebook: this.props.value.includes('facebook'),
          instagram: this.props.value.includes('instagram'),
        }
      })
    }
  }

  handleClickItem = (account) => {
    this.setState(state => ({
      selectedAccounts: {
        ...this.state.selectedAccounts,
        [account]: !this.state.selectedAccounts[account]
      }
    }))
  }

  render() {

    const itemCls = (account) =>
      classnames('social-picker__item', { 'social-picker__item_active': this.state.selectedAccounts[account] })

    return (
      <div className="social-picker">
        {services.map(account =>
          <button className={itemCls(account)} type="button" onClick={this.handleClickItem.bind(this, account)} key={account}>
            <img src={this.props.avatar} className="social-picker__avatar" alt={`${account} account avatar`}/>
            <img src={icons[account]} className="social-picker__account" alt={`${account} account avatar`} />
            <input
              type="hidden"
              name={`${this.props.name}[${account}]`}
              value={this.state.selectedAccounts[account]}
            />
          </button>
        )}
      </div>
    )
  }
}
