import nanoid from 'nanoid'
import { afterDays } from './utils'


export default () => {
  let tasks = []

  for (let i = -14; i < 14; i++) {
    tasks.push({
      id: nanoid(),
      date: afterDays(i),
      time: '10:00',
      services: ['twitter'],
      body: 'Submit your Project Loveday video by June 5 https://www.tesla.com/...',
    })

    tasks.push({
      id: nanoid(),
      date: afterDays(i),
      time: '18:00',
      services: ['facebook', 'twitter'],
      body: 'Hail cannonball blasting a Solar Roof tile http://ts.la/2r3KHsL',
    })

    tasks.push({
      id: nanoid(),
      date: afterDays(i),
      time: '11:30',
      services: ['facebook'],
      body: `"We’re mistaken when we think that technology just automatically improves...it only improves if a lot of people work very hard to make it better of people work very hard to make it better make it better of people work very hard to make it better."
  — Elon Musk, #TED2017
  Catch Elon's full TED watch`,
    })

    tasks.push({
      id: nanoid(),
      date: afterDays(i),
      time: '18:00',
      services: [],
      body: '',
    })
  }

  return { tasks }

  // return {
  //   days: [
  //     {
  //       date: new Date(),
  //       stats: {
  //         readers: 2,
  //         retweets: 129,
  //         likes: 352,
  //         replies: 14
  //       },
  //       tasks: [
  //         {
  //           id: 1,
  //           time: '10:00',
  //           services: ['twitter'],
  //           body: 'Submit your Project Loveday video by June 5 https://www.tesla.com/...',
  //         },
  //         {
  //           id: 2,
  //           time: '18:00',
  //           services: ['facebook', 'twitter'],
  //           body: 'Hail cannonball blasting a Solar Roof tile http://ts.la/2r3KHsL',
  //         }
  //       ],
  //     },
  //     {
  //       date: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 1),
  //       stats: null,
  //       tasks: [
  //         {
  //           id: 3,
  //           time: '11:30',
  //           services: ['facebook'],
  //           body: `"We’re mistaken when we think that technology just automatically improves...it only improves if a lot of people work very hard to make it better."
  // — Elon Musk, #TED2017
  // Catch Elon's full TED watch`,
  //         },
  //         {
  //           id: 4,
  //           time: '15:00',
  //           services: ['gplus', 'facebook', 'twitter'],
  //           body: `As we add more and more Teslas to the road, we’re making charging an even bigger priority. We're doubling the number of Superchargers globally and adding more destination chargers.`,
  //         },
  //         {
  //           id: 5,
  //           time: '16:20',
  //           services: ['twitter'],
  //           body: 'Happy Earth Day.',
  //         },
  //         {
  //           id: 6,
  //           time: '18:00',
  //           services: ['gplus', 'facebook', 'twitter'],
  //           body: 'Model S or Model 3: Which Tesla is right for you',
  //         },
  //         {
  //           id: 7,
  //           time: '19:00',
  //           services: ['instagram', 'facebook', 'twitter'],
  //           body: 'Solar + Storage now powering a sustainable Kauai',
  //         },
  //         {
  //           id: 8,
  //           time: '15:00',
  //           services: ['youtube', 'twitter'],
  //           body: 'Model X deliveries have started in Australia. Open the box to see the delivery',
  //         },
  //       ],
  //     }
  //   ]
  // }
}
