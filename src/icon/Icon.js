import './Icon.css'
import React from 'react'
import classnames from 'classnames'

const Icon = ({ name, className }) =>
  <span className={classnames('icon', { [className]: className })}>
    {icons[name]}
  </span>

const icons = {
  twitter: (
    <svg width="13px" height="10px" viewBox="0 0 13 10" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g id="Posts-Planned" transform="translate(-374.000000, -91.000000)" fill="#2A2A2A">
                <g id="Calendar" transform="translate(180.000000, -16.000000)">
                    <g id="Today" transform="translate(18.021429, 32.000000)">
                        <g id="Sheduled-post" transform="translate(2.000000, 60.000000)">
                            <g id="twitter-o" transform="translate(170.000000, 10.000000)">
                                <g id="Unknown">
                                    <g id="Photo">
                                        <path d="M16.2891667,6.185 C15.8358333,6.38541667 15.3516667,6.52083333 14.84125,6.58333333 C15.3620833,6.27083333 15.7629167,5.77583333 15.9504167,5.1875 C15.4633333,5.47666667 14.9245833,5.6875 14.3491667,5.79958333 C13.8879167,5.30708333 13.2316667,5 12.5079167,5 C11.1145833,5 9.98708333,6.13 9.98708333,7.52333333 C9.98708333,7.72125 10.0079167,7.91416667 10.0520833,8.09916667 C7.95583333,7.995 6.09625,6.98958333 4.85416667,5.46083333 C4.63791667,5.83333333 4.51291667,6.26833333 4.51291667,6.72916667 C4.51291667,7.60416667 4.96083333,8.3775 5.63791667,8.83083333 C5.22125,8.82041667 4.83083333,8.70583333 4.49208333,8.51541667 L4.49208333,8.54708333 C4.49208333,9.77083333 5.36208333,10.7891667 6.51541667,11.0208333 C6.30458333,11.0779167 6.08083333,11.1091667 5.85166667,11.1091667 C5.69,11.1091667 5.53125,11.09375 5.3775,11.0625 C5.69791667,12.065 6.63041667,12.7941667 7.73458333,12.815 C6.8725,13.4920833 5.78375,13.8958333 4.60166667,13.8958333 C4.39833333,13.8958333 4.19791667,13.8829167 4,13.8595833 C5.11208333,14.5833333 6.4375,15 7.85916667,15 C12.5025,15 15.0391667,11.15375 15.0391667,7.8175 C15.0391667,7.70833333 15.03625,7.59875 15.03125,7.49208333 C15.5233333,7.13541667 15.9504167,6.69291667 16.2891667,6.185" id="Logo"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  ),
  facebook: (
    <svg width="6px" height="11px" viewBox="0 0 6 11" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g id="Posts-Planned" transform="translate(-963.000000, -385.000000)" fill="#2A2A2A">
                <g id="Calendar" transform="translate(180.000000, -16.000000)">
                    <g id="Tomorrow" transform="translate(18.021429, 327.000000)">
                        <g id="Sheduled-post" transform="translate(604.249066, 60.000000)">
                            <g id="facebook-i" transform="translate(153.571606, 10.000000)">
                                <g id="Unknown">
                                    <g id="Photo">
                                        <path d="M10.8333333,7.83333333 L10.8333333,6.841 C10.8333333,6.39333333 10.9323333,6.16666667 11.6276667,6.16666667 L12.5,6.16666667 L12.5,4.5 L11.1666667,4.5 C9.5,4.5 8.83333333,5.612 8.83333333,6.83333333 L8.83333333,7.83333333 L7.5,7.83333333 L7.5,9.5 L8.83333333,9.5 L8.83333333,14.5 L10.8333333,14.5 L10.8333333,9.5 L12.302,9.5 L12.5,7.83333333 L10.8333333,7.83333333" id="Logo"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  ),
  gplus: (
    <svg width="15px" height="10px" viewBox="0 0 15 10" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g id="Posts-Planned" transform="translate(-540.000000, -386.000000)" fill="#2A2A2A">
                <g id="Calendar" transform="translate(180.000000, -16.000000)">
                    <g id="Tomorrow" transform="translate(18.021429, 327.000000)">
                        <g id="Sheduled-post" transform="translate(202.754670, 60.000000)">
                            <g id="gplus-i" transform="translate(136.508095, 10.000000)">
                                <g id="Social-Network">
                                    <g id="Group-2">
                                        <path d="M7.45454545,9.31818182 L11.6609091,9.31818182 C11.6990909,9.54090909 11.7309091,9.76363636 11.7309091,10.0563636 C11.7309091,12.6018182 10.0254545,14.4090909 7.45454545,14.4090909 C4.99181818,14.4090909 3,12.4172727 3,9.95454545 C3,7.49181818 4.99181818,5.5 7.45454545,5.5 C8.65727273,5.5 9.66272727,5.93909091 10.4390909,6.66454545 L9.23,7.82909091 C8.89909091,7.51090909 8.32,7.14181818 7.45454545,7.14181818 C5.93363636,7.14181818 4.69272727,8.40181818 4.69272727,9.95454545 C4.69272727,11.5072727 5.93363636,12.7672727 7.45454545,12.7672727 C9.21727273,12.7672727 9.87909091,11.5009091 9.98090909,10.8454545 L7.45454545,10.8454545 L7.45454545,9.31818182 Z M17,9.31818182 L17,10.5909091 L15.7272727,10.5909091 L15.7272727,11.8636364 L14.4545455,11.8636364 L14.4545455,10.5909091 L13.1818182,10.5909091 L13.1818182,9.31818182 L14.4545455,9.31818182 L14.4545455,8.04545455 L15.7272727,8.04545455 L15.7272727,9.31818182 L17,9.31818182 Z" id="sc-google-plus"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  ),
  instagram: (
    <svg width="13px" height="12px" viewBox="0 0 13 12" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g id="Posts-Planned" transform="translate(-341.000000, -585.000000)">
                <g id="Calendar" transform="translate(180.000000, -16.000000)">
                    <g id="Tomorrow" transform="translate(18.021429, 327.000000)">
                        <g id="Sheduled-post" transform="translate(2.009963, 260.000000)">
                            <g id="instagram-i" transform="translate(137.353234, 10.000000)">
                                <g id="Unknown">
                                    <g id="Photo">
                                        <rect id="Slot" stroke="#2A2A2A" strokeWidth="1.5" x="4.75" y="4.75" width="10.5" height="10.5" rx="3"></rect>
                                        <rect id="Slot" stroke="#2A2A2A" strokeWidth="1.5" x="7.75" y="7.75" width="4.5" height="4.5" rx="2.25"></rect>
                                        <rect id="Slot" fill="#2A2A2A" x="12" y="6" width="2" height="2" rx="1"></rect>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  ),
  youtube: (
    <svg width="11px" height="8px" viewBox="0 0 11 8" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g id="Posts-Planned" transform="translate(-560.000000, -587.000000)" fill="#2A2A2A">
                <g id="Calendar" transform="translate(180.000000, -16.000000)">
                    <g id="Tomorrow" transform="translate(18.021429, 327.000000)">
                        <g id="Sheduled-post" transform="translate(203.006227, 260.000000)">
                            <g id="youtube-o" transform="translate(154.522388, 10.000000)">
                                <g id="Unknown">
                                    <g id="Photo">
                                        <path d="M14.9,8.01733333 C14.9,8.01733333 14.8023333,7.32833333 14.5026667,7.02466667 C14.1223333,6.62633333 13.696,6.62433333 13.5006667,6.60133333 C12.1013333,6.5 10.002,6.5 10.002,6.5 L9.99766667,6.5 C9.99766667,6.5 7.89866667,6.5 6.49933333,6.60133333 C6.304,6.62433333 5.87766667,6.62633333 5.49733333,7.02466667 C5.19766667,7.32833333 5.1,8.01733333 5.1,8.01733333 C5.1,8.01733333 5,8.82666667 5,9.63566667 L5,10.361 C5,11.1703333 5.1,11.9793333 5.1,11.9793333 C5.1,11.9793333 5.19766667,12.6686667 5.49733333,12.972 C5.87766667,13.3703333 6.37733333,13.3576667 6.6,13.3996667 C7.4,13.4763333 10,13.5 10,13.5 C10,13.5 12.1013333,13.4966667 13.5006667,13.3956667 C13.696,13.3723333 14.1223333,13.3703333 14.5026667,12.972 C14.8023333,12.6686667 14.9,11.9793333 14.9,11.9793333 C14.9,11.9793333 15,11.1703333 15,10.361 L15,9.63566667 C15,8.82666667 14.9,8.01733333 14.9,8.01733333 L14.9,8.01733333 Z M8.96766667,11.3136667 L8.967,8.504 L11.6693333,9.914 L8.96766667,11.3136667 L8.96766667,11.3136667 Z" id="Fill-11"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  ),
  close: (
    <svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg"
xmlnsXlink="http://www.w3.org/1999/xlink">
        <defs>
            <path d="M12,11.2928932 L5,4.29289322 L4.29289322,5 L11.2928932,12 L4.29289322,19 L5,19.7071068 L12,12.7071068 L19,19.7071068 L19.7071068,19 L12.7071068,12 L19.7071068,5 L19,4.29289322 L12,11.2928932 Z"
            id="path-1" />
        </defs>
        <g id="Page-1" fill="none" fillRule="evenodd">
            <g id="Posts-Creation" transform="translate(-768 -56)">
                <g id="Popup" transform="translate(210 30)">
                    <g id="Header">
                        <g id="Icon-/-Cross-Tiny" transform="translate(554 22)">
                            <mask id="mask-2" fill="#fff">
                                <use xlinkHref="#path-1" />
                            </mask>
                            <use id="Mask" fill="#9B9B9B" fillRule="nonzero" xlinkHref="#path-1" />
                            <g id="Color-/-Black" mask="url(#mask-2)" fill="#2A2A2A">
                                <rect id="Rectangle-3" width="24" height="24" />
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  ),
  plus: (
    <svg width='13' height='12' viewBox='0 0 13 12' xmlns='http://www.w3.org/2000/svg'>
        <g id='Page-1' fill='none' fillRule='evenodd'>
            <g id='Posts-Planned' transform='translate(-694 -193)' fill='#2A2A2A'>
                <g id='Calendar' transform='translate(180 -16)'>
                    <g id='Today' transform='translate(18.021 32)'>
                        <g id='Slot-3' transform='translate(402.479 60)'>
                            <g id='plus-i' transform='translate(88.105 111)'>
                                <g id='Plus-Icon' transform='translate(4 4)'>
                                    <path d='M7,7 L2,7 L2,9 L7,9 L7,14 L9,14 L9,9 L14,9 L14,7 L9,7 L9,2 L7,2 L7,7 Z'
                                    id='Combined-Shape' />
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  ),
  trigger: (
    <svg width='6' height='6' viewBox='0 0 6 6' xmlns='http://www.w3.org/2000/svg'>
        <g id='Page-1' fill='none' fillRule='evenodd'>
            <g id='Posts-Creation' transform='translate(-502 -128)' fill='#A6A6A6'>
                <g id='Popup' transform='translate(210 30)'>
                    <g id='Date' transform='translate(20 86)'>
                        <g id='Date-Picker' transform='translate(118)'>
                            <polygon id='Triangle' points='157 18 160 12 154 12' />
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  ),
  readers: (
    <svg width='20' height='20' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'>
      <g id='Page-1' fill='none' fillRule='evenodd'>
        <g id='Posts-Planned' transform='translate(-782 -31)'>
          <g id='Calendar' transform='translate(180 -16)'>
            <g id='Today' transform='translate(18.021 32)'>
              <g id='Stat' transform='translate(584 15)'>
                  <g id='Readers'>
                      <g id='Group-3'>
                          <circle id='Oval-3' fill='#CDCCCC' cx='10' cy='10' r='10' />
                          <path d='M10,0 C13.1592793,0 15.9763784,1.46504796 17.8089741,3.75282075 L10,10 L10,0 Z'
                          id='Combined-Shape' fill='#2A2A2A' />
                          <path d='M4.5,15.7121653 C4.87184807,15.4262051 5.39358769,15.1948372 5.96519534,14.9413565 C6.83269082,14.5565326 7.91261344,14.0777635 7.91261344,13.510073 L7.91261344,12.7374109 C7.60515312,12.4827037 7.08424926,11.9351084 7.00242925,11.1117059 C6.7534118,10.8796059 6.34024611,10.4063629 6.34024611,9.82058665 C6.34024611,9.46490087 6.48254179,9.17804131 6.60755872,8.99567699 C6.53132889,8.61436979 6.3880168,7.81608633 6.3880168,7.23031004 C6.3880168,5.26800973 7.77031775,4 9.90932683,4 C10.5232311,4 11.2687588,4.16277145 11.6839573,4.60235486 C12.6536008,4.77115489 13.4311451,5.90603364 13.4311451,7.23031004 C13.4311451,8.07983637 13.28021,8.77864838 13.1892424,9.12428653 C13.290374,9.29358894 13.3996367,9.54477946 13.3996367,9.83616046 C13.3996367,10.494782 13.0632091,10.9208011 12.7491422,11.1483797 C12.6602074,11.9602275 12.1921562,12.4837085 11.9065484,12.7318847 L11.9065484,13.510073 C11.9065484,13.9928611 12.7918309,14.3173993 13.648146,14.6313874 C14.2888281,14.866083 15.0377285,15.2178371 15.5,15.5846229 C15.1950602,15.7938967 13.2116032,17.9999998 9.90958093,18 C6.60755865,18.0000002 4.87184807,16.0641562 4.5,15.7121653 Z'
                          id='Fill-58' fill='#FFF' />
                      </g>
                  </g>
              </g>
            </g>
          </g>
        </g>
      </g>
    </svg>
  ),
  retweets: (
    <svg width='20' height='20' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'>
      <g id='Page-1' fill='none' fillRule='evenodd'>
          <g id='Posts-Planned' transform='translate(-836 -31)'>
              <g id='Calendar' transform='translate(180 -16)'>
                  <g id='Today' transform='translate(18.021 32)'>
                      <g id='Stat' transform='translate(584 15)'>
                          <g id='Retweets' transform='translate(54)'>
                              <g id='Group-4'>
                                  <circle id='Oval-3' fill='#CDCCCC' cx='10' cy='10' r='10' />
                                  <path d='M10,0 C15.5228475,0 20,4.4771525 20,10 C20,11.8819166 19.4801518,13.6424162 18.5762213,15.1457328 L10,10 L10,0 Z'
                                  id='Combined-Shape' fill='#2A2A2A' />
                                  <rect id='Rectangle-13' fill='#CDCCCC' x='6' y='7' width='8' height='6'
                                  />
                                  <path d='M4.50265045,9.08842926 L2.67422333,10.9168564 L1.26000977,9.50264282 L5.50265045,5.26000214 L9.74529114,9.50264282 L8.33107758,10.9168564 L6.50265045,9.08842926 L6.50265045,11.5072453 C6.50265045,12.0469784 6.95147303,12.5026428 7.50512375,12.5026428 L10.5026505,12.5026428 L12.5026505,14.5026428 L6.51118065,14.5026428 C5.40190005,14.5026428 4.50265045,13.6024667 4.50265045,12.5106258 L4.50265045,9.08842926 Z M15.5026505,10.9168564 L17.3310776,9.08842926 L18.7452911,10.5026428 L14.5026505,14.7452835 L10.2600098,10.5026428 L11.6742233,9.08842926 L13.5026505,10.9168564 L13.5026505,8.49804039 C13.5026505,7.95830722 13.0538279,7.50264282 12.5001772,7.50264282 L9.50265045,7.50264282 L7.50265045,5.50264282 L13.4941203,5.50264282 C14.6034009,5.50264282 15.5026505,6.40281899 15.5026505,7.49465984 L15.5026505,10.9168564 Z'
                                  id='Combined-Shape' fill='#FFF' />
                              </g>
                          </g>
                      </g>
                  </g>
              </g>
          </g>
      </g>
    </svg>
  ),
  likes: (
    <svg width='20' height='20' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'>
        <g id='Page-1' fill='none' fillRule='evenodd'>
            <g id='Posts-Planned' transform='translate(-899 -31)'>
                <g id='Calendar' transform='translate(180 -16)'>
                    <g id='Today' transform='translate(18.021 32)'>
                        <g id='Stat' transform='translate(584 15)'>
                            <g id='Likes' transform='translate(117)'>
                                <circle id='Oval-3' fill='#CDCCCC' cx='10' cy='10' r='10' />
                                <path d='M10,16 L9.79196692,15.8221125 C5.23437685,11.9242168 4,10.552101 4,8.32329657 C4,6.49083159 5.41972829,5 7.16479622,5 C8.62315416,5 9.44748966,5.86971378 10,6.52395291 C10.5525103,5.86971378 11.3768458,5 12.8352038,5 C14.5802717,5 16,6.49083159 16,8.32329657 C16,10.552101 14.7656232,11.9242168 10.2080331,15.8221125 L10,16 L10,16 Z'
                                id='Fill-18' fill='#FFF' />
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  ),
  replies: (
    <svg width='20' height='20' viewBox='0 0 20 20' xmlns='http://www.w3.org/2000/svg'>
        <g id='Page-1' fill='none' fillRule='evenodd'>
            <g id='Posts-Planned' transform='translate(-962 -31)'>
                <g id='Calendar' transform='translate(180 -16)'>
                    <g id='Today' transform='translate(18.021 32)'>
                        <g id='Stat' transform='translate(584 15)'>
                            <g id='Replies' transform='translate(180)'>
                                <g id='Group-5'>
                                    <circle id='Oval-3' fill='#CDCCCC' cx='10' cy='10' r='10' />
                                    <path d='M2.92893219,17.0710678 C4.73857625,18.8807119 7.23857625,20 10,20 C15.5228475,20 20,15.5228475 20,10 C20,4.4771525 15.5228475,0 10,0 L10,10 L2.92893219,17.0710678 Z'
                                    id='Combined-Shape' fill='#2A2A2A' />
                                    <path d='M6.45390071,16 L5.81241135,16 L6.19007092,15.4949121 C6.47588652,15.1131595 6.66276596,14.6263819 6.75851064,14.0117776 C4.97695035,13.2358354 4,11.7523555 4,9.80904523 C4,6.84277638 6.29929078,5 10,5 C13.7007092,5 16,6.84277638 16,9.80904523 C16,12.8202261 13.7570922,14.6180905 10,14.6180905 C9.91631206,14.6180905 9.83368794,14.6160176 9.75141844,14.6129083 C9.18794326,15.2744975 8.17695035,16 6.45390071,16 L6.45390071,16 Z'
                                    id='Fill-59' fill='#FFF' />
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  )
}

export default Icon
