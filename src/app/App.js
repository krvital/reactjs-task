import './App.css'
import React from 'react'
import Page from '../page/Page'
import PlanPage from '../plan-page/PlanPage'
import data from '../data'

const fakeRequest = () =>
  new Promise(resolve =>
    setTimeout(() => resolve({ status: 'success'}), 1000)
  )

const App = () => (
  <div className="app">
    <Page>
      <PlanPage
        tasks={data().tasks}
        onSaveDraft={fakeRequest}
        onSaveTask={() => {}}
      />
    </Page>
  </div>
)

export default App
