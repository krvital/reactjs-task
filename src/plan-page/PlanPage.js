import './PlanPage.css'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { afterDays, isSameDays } from '../utils'
import Day from '../day/Day'
import Modal from '../modal/Modal'
import FormTask from '../form-task/FormTask'


export default class PlanPage extends Component {
  state = {
    activeTaskId: null,
    editing: false,
    isTaskSaving: false,
    isTaskSaved: false,
  }

  static propTypes = {
    tasks: PropTypes.array,
  }

  componentDidMount() {
    this.scrollToToday()
  }

  scrollToToday() {
    if ('scrollRestoration' in window.history) {
      window.history.scrollRestoration = 'manual';
    }
    const y = document.querySelector('[name="today"]').offsetTop
    window.scrollTo(0, y)
  }

  generateDays() {
    let days = []

    for (let i = -14; i <= 14; i++) {
      days.push({
        date: afterDays(i),
        stats: i < 1
          ? { readers: 2, retweets: 129, likes: 352, replies: 14 }
          : null,
      })
    }

    return days
  }

  filterTasksByday = (tasks, day) =>
    tasks.filter(task => isSameDays(task.date, day.date))

  findTaskById = (tasks, taskId) =>
    tasks.find(task => task.id === taskId)

  activeTask = () =>
    this.findTaskById(this.props.tasks, this.state.activeTaskId)

  handleClickTask = (taskId, e) => {
    this.setState({ editing: true, activeTaskId: taskId })
  }

  handleClickNewTask = () =>
    this.setState({ editing: true, activeTaskId: null })

  handleCloseModal = () =>
    this.setState({ editing: false, activeTaskId: null, isTaskSaving: false, isTaskSaved: false })

  handleSaveTask = () => {
    this.handleCloseModal()
  }

  handleSaveDraft = (values) => {
    this.setState({ isTaskSaving: true })

    this.props.onSaveDraft(values)
      .then(response => {
        this.setState({ isTaskSaving: false, isTaskSaved: true })
        setTimeout(this.handleCloseModal, 500)
      })
  }

  render() {
    return (
      <div className="plan-page">
        {this.generateDays().map((day, index) => (
          <Day
            key={index}
            tasks={this.filterTasksByday(this.props.tasks, day)}
            activeTaskId={this.state.activeTaskId}
            onClickTask={this.handleClickTask}
            onClickNewTask={this.handleClickNewTask}
            {...day}
          />
        ))}
        <Modal
          title={this.activeTask() ? 'Update post' : 'New post'}
          open={this.state.editing}
          onClose={this.handleCloseModal}
          startPoint={this.state.activeTaskBounds}
        >
          <FormTask
            isSaving={this.state.isTaskSaving}
            isSaved={this.state.isTaskSaved}
            values={this.activeTask()}
            onSaveTask={this.handleSaveTask}
            onSaveDraft={this.handleSaveDraft}
          />
        </Modal>
      </div>
    )
  }
}
