import './Textarea.css'
import React from 'react'
import inputHOC from '../input-hoc'


const Textarea = (props) => {
  const { onChangeValue, ...textareaProps } = props
  return (
    <textarea
      className="textarea"
      onChange={(e) => props.onChangeValue(e.target.value)}
      {...textareaProps}
    ></textarea>
  )
}

export default inputHOC(Textarea, { defaultValue: '' })
