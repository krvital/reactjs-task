import './DayPicker.css';
import './DatePicker.css';
import React from 'react'
import DayPicker from 'react-day-picker'
import Dropdown from '../dropdown/Dropdown'
import inputHOC from '../input-hoc'
import { formatDate, today } from '../utils'

const DatePicker = (props) => (
  <div className="date-picker">
    <Dropdown value={formatDate(props.value)}>
      <DayPicker
        selectedDays={props.value}
        onDayClick={props.onChangeValue}
        disabledDays={{ before: today() }}
        fromMonth={today()}
      />
    </Dropdown>
    <input
      type="hidden"
      name={props.name}
      value={props.value.toISOString().slice(0, 10)}
    />
  </div>
)

export default inputHOC(DatePicker, { defaultValue: new Date() })
