import './TimePicker.css'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Dropdown from '../dropdown/Dropdown'

const hours = [...Array(24)].map((v, i) => `${i}`.padStart(2, '0'))
const minutes = [...Array(60)].map((v, i) => `${i}`.padStart(2, '0'))

export default class TimePicker extends Component {
  static propTypes = {
    value: PropTypes.string
  }

  static defaultProps = {
    value: '00:00'
  }

  constructor(props) {
    super(props)
    this.state = {
      value: props.value,
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.value !== prevProps.value) {
      this.setState({
        value: this.props.value
      })
    }
  }

  getHours = (timeStr) => timeStr.split(':')[0]
  getMinutes  = (timeStr) => timeStr.split(':')[1]

  handleSelectHour = (h) =>
    this.setState(state => ({ value: `${h}:${this.getMinutes(state.value)}` }))

  handleSelectMinute = (m) =>
    this.setState(state => ({ value: `${this.getHours(state.value)}:${m}` }))

  render() {
    const { value } = this.state
    const hour = this.getHours(value)
    const minute = this.getMinutes(value)

    return (
      <div className="time-picker">
        <Dropdown className="time-picker__hours" value={hour}>
          <div className="time-picker__list">
            {hours.map(h =>
              <div
                className={classnames(
                  'time-picker__item', { 'time-picker__item_active': hour === h}
                )}
                onClick={this.handleSelectHour.bind(this, h)}
                key={h}
              >
                {h}
              </div>
            )}
          </div>
        </Dropdown>
        <Dropdown className="time-picker__minutes" value={minute}>
          <div className="time-picker__list">
            {minutes.map(m =>
              <div
                className={classnames(
                  'time-picker__item', { 'time-picker__item_active': minute === m}
                )}
                onClick={this.handleSelectMinute.bind(this, m)}
                key={m}
              >
                {m}
              </div>
            )}
          </div>
        </Dropdown>

        <input type="hidden" name={this.props.name} value={this.state.value} />
      </div>
    )
  }
}
