export function weekday(date) {
  return date.toLocaleString('en-US', { weekday: 'long'})
}

export function dayName(date) {
  if (isToday(date)) {
    return 'Today'
  } else if ((date.toDateString() === tomorrow().toDateString())) {
    return 'Tomorrow'
  } else if ((date.toDateString() === yesterday().toDateString())) {
    return 'Yesterday'
  } else {
    return date.toLocaleDateString('en-US')
  }
}

export function isToday(date) {
  return isSameDays(date, today())
}

function now() {
  const dt = new Date()
  dt.setHours(0,0,0,0)
  return dt
}

export function today() {
  const dt = new Date()
  dt.setHours(0,0,0,0)
  return dt
}

export function yesterday() {
  const today = now()
  return new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1)
}

export function tomorrow() {
  const today = now()
  return new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1)
}

export function afterDays(daysCount) {
  const today = now()
  return new Date(today.getFullYear(), today.getMonth(), today.getDate() + daysCount)
}

export function formatDate(date) {
  return date.toLocaleDateString('en-US', { month: 'short', day: 'numeric', weekday: 'short' })
}

export function isSameDays(a, b) {
  const aDay = new Date(a.getTime())
  const bDay = new Date(b.getTime())
  aDay.setHours(0,0,0,0)
  bDay.setHours(0,0,0,0)
  return aDay.toString() === bDay.toString()
}

export function isPast(date) {
  const d = new Date(date.getTime())
  d.setHours(0,0,0,0)
  return d < today() 
}
