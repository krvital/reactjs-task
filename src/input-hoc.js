import React, { Component } from 'react'

const inputHOC = (WrappedComponent, params = {}) =>
  class Input extends Component {
    constructor(props) {
      super(props)
      this.state = { value: props.value || params.defaultValue }
    }

    componentDidUpdate(prevProps, prevState) {
      if (this.props.value !== prevProps.value) {
        this.setState({ value: this.props.value || params.defaultValue })
      }
    }

    handleChangeValue = (value) => {
      this.setState({ value })
    }

    render() {
      return <WrappedComponent {...this.props} value={this.state.value} onChangeValue={this.handleChangeValue} />
    }
  }


export default inputHOC
