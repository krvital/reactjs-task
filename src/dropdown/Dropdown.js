import './Dropdown.css'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Icon from '../icon/Icon'

export default class Dropdown extends Component {
  static propTypes = {
    open: PropTypes.bool,
    value: PropTypes.string,
  }

  constructor(props) {
    super(props)

    this.state = {
      open: props.open
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.open !== prevProps.open) {
      this.setState({ open: this.props.open })
    }

    if (this.state.open !== prevState.open) {
      if (this.state.open) {
        document.addEventListener('click', this.handleClickOutside);
      } else {
        document.removeEventListener('click', this.handleClickOutside);
      }
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside);
  }

  handleClickOutside = (e) => {
    if (this.rootNode !== e.target && !this.rootNode.contains(e.target) && this.state.open) {
      this.setState({ open: false })
    }
  }

  handleClickTrigger = () => {
    this.setState(state => ({ open: !state.open }))
  }

  render() {
    const dropdownCls = classnames('dropdown', {
      [this.props.className]: this.props.className,
      'dropdown_opened': this.state.open
    })

    return (
      <div className={dropdownCls} ref={c => this.rootNode = c}>
        <button
          type="button"
          className="dropdown__trigger"
          ref={c => this.trigger = c}
          onClick={this.handleClickTrigger}
        >
          {this.props.value}
          <Icon name="trigger" className="dropdown__trigger-icon" />
        </button>
        <div className="dropdown__popup">
          {this.props.children}
        </div>
      </div>
    )
  }
}
