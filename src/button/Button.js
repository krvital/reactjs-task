import './Button.css'
import React from 'react'
import classnames from 'classnames'
import Loader from 'react-loader'

const iconCheck = (
  <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
    <g fill="none" fillRule="evenodd">
      <g fillRule="nonzero" fill="#FFF">
        <path d="M10,0 C4.476875,0 0,4.476875 0,10 C0,15.523125 4.476875,20 10,20 C15.5225,20 20,15.5225 20,10 C20,4.476875 15.5225,0 10,0 Z M10,18.75 C5.1675,18.75 1.25,14.8325 1.25,10 C1.25,5.1675 5.1675,1.25 10,1.25 C14.8325,1.25 18.75,5.1675 18.75,10 C18.75,14.8325 14.8325,18.75 10,18.75 Z"
        />
        <path d="M14.5625,6.495625 L8.1325,12.868125 L5.456875,10.245625 C5.210625,10.00125 4.810625,10.00125 4.564375,10.245625 C4.3175,10.49 4.3175,10.885625 4.564375,11.129375 L7.68875,14.19125 C7.945,14.416875 8.328125,14.441875 8.58125,14.19125 L15.455625,7.379375 C15.701875,7.135625 15.701875,6.739375 15.455625,6.495625 C15.20875,6.251875 14.809375,6.251875 14.5625,6.495625 Z"
        />
      </g>
    </g>
  </svg>
)

const Button = ({ disabled, isLoading, isSuccess, title, ...buttonProps }) => {
  const btnDisabled = disabled || isLoading || isSuccess

  const btnCls = classnames('button', {
    'button_default': !btnDisabled,
    'button_disabled': disabled,
    'button_loading': isLoading,
    'button_success': isSuccess
  })

  return (
    <button className={btnCls} type="button" disabled={btnDisabled} {...buttonProps}>
      {isLoading && <Loader loaded={false} scale={0.6} length={2} width={5} />}
      <div className="button__icon-check">{iconCheck}</div>
      <span className="button__text">{title}</span>
    </button>
  )
}

export default Button
