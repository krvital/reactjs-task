import './Modal.css'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Icon from '../icon/Icon'

export default class Modal extends Component {
  static propTypes = {
    open: PropTypes.bool,
    onClose: PropTypes.func
  }

  static defaultProps = {
    onClose: () => {}
  }

  componentDidMount() {
    document.addEventListener('keyup', e => {
      if (e.key === 'Escape') {
        this.props.onClose()
      }
    })

    if (this.props.open) {
      document.querySelector('body').style.overflow = 'hidden'
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.open !== prevProps.open) {
      document.querySelector('body').style.overflow = this.props.open ? 'hidden' : 'visible'
    }
  }

  handleClickClose = (e) => {
    e.preventDefault()
    this.props.onClose()
  }

  render() {
    return (
      <div className={classnames('modal', { 'modal_open': this.props.open, 'modal_closed': !this.props.open })}>
        <div className="modal__backdrop" onClick={this.props.onClose}></div>
        <div className="modal__window">
          <div className="modal__head">
            <span className="modal__title">{this.props.title}</span>
            <button className="modal__close-button" onClick={this.handleClickClose}>
              <Icon name="close" />
            </button>
          </div>
          <div className="modal__content">
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
}
