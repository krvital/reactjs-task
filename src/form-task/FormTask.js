import './FormTask.css'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import serialize  from 'form-serialize'
import Button from '../button/Button'
import DatePicker from '../date-picker/DatePicker'
import TimePicker from '../time-picker/TimePicker'
import Textarea from '../textarea/Textarea'
import SocialPicker from '../social-picker/SocialPicker'


export default class FormTask extends Component {
  static propTypes = {
    values: PropTypes.object,
    isSaving: PropTypes.bool,
    isSaved: PropTypes.bool,
    onSaveDraft: PropTypes.func,
    onSaveTask: PropTypes.func,
  }

  static defaultProps = {
    values: {},
    isSaving: false,
    isSaved: false,
    onSubmit: () => {}
  }

  handleSaveDraft = (e) => {
    e.preventDefault()
    const values = serialize(this.form, { hash: true })
    this.props.onSaveDraft(values)
  }

  handleSave = (e) => {
    e.preventDefault()
    const values = serialize(this.form, { hash: true })
    this.props.onSaveTask(values)
  }

  render() {
    const { date, time, services, body } = this.props.values

    return (
      <form className="form-task" ref={c => { this.form = c }}>
        <div className="form-task__body">
          <div className="form-task__datetime">
            When to publish:
            <div className="form-task__date">
              <DatePicker value={date} name="date" />
            </div>
            <span className="form-task__datetime-at">at</span>
            <div className="form-task__time">
              <TimePicker value={time} name="time" />
            </div>
          </div>
          <div className="form-task__social">
            <SocialPicker value={services} name="services" avatar="/tesla.png" />
          </div>
          <div>
            <Textarea name="text" rows="6" placeholder="Text and links" value={body || ''}></Textarea>
          </div>
        </div>
        <div className="form-task__footer form-task__footer_sides">
          <Button
            title="Save as draft"
            onClick={this.handleSaveDraft}
            isSuccess={this.props.isSaved}
            isLoading={this.props.isSaving}
          />
          <Button
            title="Schedule post"
            onClick={this.handleSave}
            disabled
          />
        </div>
      </form>
    )
  }
}
