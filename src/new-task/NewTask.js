import './NewTask.css'
import React from 'react'
import PropTypes from 'prop-types'
import Icon from '../icon/Icon'

const NewTask = (props) => (
  <div className="new-task" onClick={props.onClick} tabIndex="1">
    <div>
      <div>Schedule post<br/>on this day</div>
      <div className="new-task__plus"><Icon name='plus' /></div>
    </div>
  </div>
)

NewTask.propTypes = {
  onClick: PropTypes.func.isRequired,
}

export default NewTask
